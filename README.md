### A SpringBoot Application with BoneCP

Demonstrates how to set up a connection pool with [BoneCP](https://github.com/wwadge/bonecp) in a SpringBoot application. 

### To run the application
`mvn clean package && java -jar target/springbootbonecpds.war`

### Maven dependecy tree:
`mvn help:effective-pom -Doutput=result.xml`

### Eclipse project structure:
[Show picture](https://bitbucket.org/georg7/springbootbonecp/src/master/doc/springbootcp_eclipseprojectstructure.png?raw=true)

### Eclipse Java build path:
[Show picture](https://bitbucket.org/georg7/springbootbonecp/src/master/doc/springbootcp_javabuildpath.png?raw=true)