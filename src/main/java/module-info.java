module springbootbonecpds { //com.test.so.common
    //exports com.test.so.repository;
    //exports com.test.so;
    //exports com.test.so.service;
    //exports com.test.so.common;
    //exports com.test.so.domain;
    //exports com.test.so.controller;
    //exports com.test.so.service.impl;

    requires bonecp;
    requires com.fasterxml.jackson.core;
    requires com.fasterxml.jackson.databind;
    requires java.naming;
    requires java.persistence;
    requires java.sql;
    requires java.ws.rs;
    requires java.xml;
    requires lombok;
    requires org.apache.log4j;
    requires org.apache.tomcat.embed.core;
    requires org.hibernate.orm.core;
    requires org.json;
    requires org.slf4j;
    requires spring.beans;
    requires spring.boot;
    requires spring.boot.autoconfigure;
    requires spring.context;
    requires spring.core;
    requires spring.data.commons;
    requires spring.data.jpa;
    requires spring.orm;
    requires spring.tx;
    requires spring.web;
    requires tlms.entities;
    requires tlms.services.non.spring;
    requires webapp;
}