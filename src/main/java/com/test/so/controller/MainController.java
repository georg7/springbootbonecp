package com.test.so.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.test.so.common.ContextHolder;
import com.test.so.common.TLMSRights;
import com.tieto.springbasics.jsonapi.model.Document;
import com.tieto.springbasics.jsonapi.model.Resource;
//import com.tieto.cs.jpa.ContextHolder;
import com.tieto.tlms_entities.model.TerminalType;
import com.tieto.tlms_services_non_spring.service.TlmsService;

import lombok.extern.slf4j.Slf4j;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by azizunsal on 24/02/15.
 */

@Slf4j
@RestController
@RequestMapping("/")
public class MainController {
    private static final String PERSISTENCE_CONTEXT = "tlms";
    
    //@Autowired
    //private PhoneSettingsService phoneSettingsService;
    
    private static final Logger logger = LoggerFactory.getLogger(MainController.class);
    
    @RequestMapping(method = RequestMethod.GET)
    public String test() {
        logger.info("Hello controller - test");
        return "Hello Controller - Test";
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String testById(@PathVariable Long id) {
        logger.info("Hello controller - test by id {}", id);
        return "Hello Controller - Test By Id";
    }
    
    //@GET
    //@Path("terminalTypes")
    @RequestMapping(value = "/terminalTypes", method = RequestMethod.GET)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllTerminalTypes(@Context HttpServletRequest request) throws Exception {
        TLMSRights rights = new TLMSRights(request.getSession());
        if (!rights.applicationAllowed()) {
            //return Response.status(Response.Status.FORBIDDEN).entity("User not allowed to access TLMS").build();
        }
        
        //Resource res = mapper.readValue(body, Document.class).getOne();

        EntityManager em = ContextHolder.getManager(PERSISTENCE_CONTEXT)
            .orElseThrow(() -> new Exception("Failed to get persistence context!"));
        
        
        //private EntityManager em = ContextHolder.getManager(PERSISTENCE_CONTEXT);

        try {
            TlmsService tlmsService = new TlmsService(em);
            List<TerminalType> tTypes = tlmsService.getAllTerminalTypes();

            ObjectMapper objectMapper = new ObjectMapper();
            JSONObject json = new JSONObject();
            json.put("terminalTypes", new JSONArray(objectMapper.writeValueAsString(tTypes)));
            return Response.ok(json.toString()).build();
        } catch (Exception e) {
            log.debug("RestApi::terminalTypes::returnResponse::err: " + e.toString());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("Internal server error calling GET domains").build();
        } finally {
            em.close();
        }
    }
}
