package com.test.so.common;

import lv.tietoenator.cs.util.CsDocumentBuilderFactory;
import lv.tietoenator.cs.auth.PasswordFetcher;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.util.*;

/**
 * Holds JPA EntityManagers for each persistence-unit
 *   configured in persistence.xml
 *
 * Requires persistence.xml parsing since we need to get user_password from password_wallet
 *     and there is no easy way to use existing context.xml jdbc resources.
 */
public final class ContextHolder {
    private final static Logger log = LogManager.getLogger(ContextHolder.class);
    private static boolean initialized = false;
    private static HashMap<String, EntityManagerFactory> factories = new HashMap<>();

    /**
     * Parses persistence.xml file, gets jdbc password,
     *   creates EntityManager and inserts into map of managers.
     */
    synchronized private static void init() {
        //String jpaFile = Environment.getCorePath() + "target/WEB-INF/classes/META-INF/persistence.xml";
        String jpaFile = "C:\\Users\\grigpjur\\eclipse-workspace\\ACQ_REACT_TLMS\\SpringBoot-BoneCP-PooledDataSource\\target\\springbootbonecpds\\WEB-INF\\classes\\META-INF\\persistence.xml";
        //log.info("JG: " + context.);
        try {
            getJpaCredentials(jpaFile).forEach( (k, v) -> {
                factories.put(k,
                        Persistence.createEntityManagerFactory(k,
                                Collections.singletonMap("javax.persistence.jdbc.password",
                                        PasswordFetcher.getPassword(v.getKey(), v.getValue())
                                )
                        )
                );
            });
            log.info("JPA providers initialized.");
            initialized = true;
        } catch (Exception e) {
            log.error("Failed to initialize JPA Providers.", e);
        }
    }

    /**
     * Returns requested EntityManager instance.
     *    In case not initialized yet, initializes all configured EntityManagers.
     * @param context - persistence context name of required EntityManager
     * @return Optional EntityManager - returns EntityManager if it exists (is initialized)
     */

    public static Optional<EntityManager> getManager(String context) {
        if (!initialized) { init(); }

        if (!factories.isEmpty()) {
            if (factories.containsKey(context)) {
                return Optional.of(factories.get(context).createEntityManager());
            } else {
                log.error("No JPA provider for context " + context + " initialized!");
                return Optional.empty();
            }
        } else {
            log.error("No JPA providers initialized!");
            return Optional.empty();
        }
    }

    /**
     * Parses given XML file and extracts JPA JDBC urls and usernames
     *
     * @param fileName - full path to persistence.xml
     * @return map of units and pairs of url and username
     */
    private static Map<String, AbstractMap.SimpleEntry<String, String>> getJpaCredentials(String fileName) {
        Map<String, AbstractMap.SimpleEntry<String, String>> res = new HashMap<>();

        try {
            Document cfg = CsDocumentBuilderFactory.newInstance().newDocumentBuilder().parse(fileName);

            // <persistence>
            //   <persistence-unit name=...
            //   ...
            NodeList nl = cfg.getElementsByTagName("persistence-unit");
            for (int i = 0; i < nl.getLength(); i ++ ) {
                String context;

                if (nl.item(i).getNodeType() == Node.ELEMENT_NODE) {
                    Element el = (Element) nl.item(i);

                    context = el.getAttribute("name");

                    // <persistence>
                    //   <persistence-unit name=...
                    //   ...
                    //      <properties>
                    //         <property ...
                    //         <property ...
                    //         ...
                    Node n = el.getElementsByTagName("properties").item(0);
                    NodeList nll = ((Element) n).getElementsByTagName("property");
                    String userName = null;
                    String url = null;
                    for (int j = 0; j < nll.getLength(); j++) {
                        Node nn = nll.item(j);
                        if (nn.getNodeType() == Node.ELEMENT_NODE) {
                            Element ell = (Element) nn;

                            if (ell.getAttribute("name").equals("javax.persistence.jdbc.url")) {
                                url = ell.getAttribute("value");
                            } else if (ell.getAttribute("name").equals("javax.persistence.jdbc.user")) {
                                userName = ell.getAttribute("value");
                            }

                            if (userName != null && url != null) { // we have everything we wanted
                                res.put(context, new AbstractMap.SimpleEntry<>(url, userName));
                                break;
                            }
                        }
                    }
                }
            }
            log.debug("Parsed JPA configuration file " + fileName);
        } catch (Exception e) {
            log.error("Failed to parse JPA configuration.");
        }
        return res;
    }
}
