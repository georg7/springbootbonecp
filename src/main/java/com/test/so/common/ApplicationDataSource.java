package com.test.so.common;

import com.jolbox.bonecp.BoneCPDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.LocalEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import java.util.Properties;

import javax.sql.DataSource;

/**
 * Created by azizunsal on 24/02/15.
 */

@Configuration
@PropertySource("classpath:application.properties")
public class ApplicationDataSource {
    
    @Autowired
    private Environment environment;

    
    @Bean
    public DataSource dataSource() {
        BoneCPDataSource dataSource = new BoneCPDataSource();
        dataSource.setDriverClass(environment.getRequiredProperty("spring.datasource.driverClassName"));
        dataSource.setJdbcUrl(environment.getRequiredProperty("spring.datasource.url"));
        dataSource.setUsername(environment.getRequiredProperty("spring.datasource.username"));
        dataSource.setPassword(environment.getRequiredProperty("spring.datasource.password"));
        return dataSource;
    }
    
    @Bean
    public LocalEntityManagerFactoryBean entityManagerFactoryBean() {
        LocalEntityManagerFactoryBean factory = new LocalEntityManagerFactoryBean();
        factory.setPersistenceUnitName("tlms");
        return factory;
    }
    
    /*
     * @Bean public LocalContainerEntityManagerFactoryBean
     * someEntityManagerFactory() { LocalContainerEntityManagerFactoryBean em = new
     * LocalContainerEntityManagerFactoryBean(); em.setDataSource(dataSource());
     * em.setPackagesToScan(new String[] { "com.tieto.mda_entities.model.lcm" });
     * JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
     * em.setJpaVendorAdapter(vendorAdapter);
     * em.setJpaProperties(hibernateProperties());
     * 
     * return em; }
     */
    
    //@Bean(name="entityManagerFactory")
    public LocalSessionFactoryBean sessionFactory() {
        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
        sessionFactory.setDataSource(dataSource());
        sessionFactory.setPackagesToScan(new String[] { "com.tieto.mda_entities.model.lcm" });
        sessionFactory.setHibernateProperties(hibernateProperties());
        //sessionFactory.
        return sessionFactory;
    } 
    
    Properties hibernateProperties() {
        return new Properties() {
           /**
           * 
           */
          private static final long serialVersionUID = 1L;

          {
              setProperty("hibernate.hbm2ddl.auto", "create");
              setProperty("hibernate.show_sql", "false");
              setProperty("hibernate.dialect", "org.hibernate.dialect.Oracle12cDialect");
           }
        };
     }
}
